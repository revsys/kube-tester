# vim: ft=python
# pylint: disable=unused-wildcard-import,missing-docstring,wildcard-import,unused-import,undefined-variable,relative-beyond-top-level

from os import environ as Env
from kube_tester.settings import *


DEBUG = False if not Env.get('DJANGO_DEBUG', '') else True

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


ALLOWED_HOSTS += [
    Env['DJANGO_FQDN'],
]

SECRET_KEY = Env['DJANGO_SECRET_KEY']

DATABASES = {
    'default': {
        'ATOMIC_REQUESTS': False,
        'AUTOCOMMIT': True,
        'CONN_MAX_AGE': 0,
        'ENGINE': 'django_db_geventpool.backends.postgresql_psycopg2',
        'HOST': Env['PGHOST'],
        'NAME': Env['PGDB'],
        'PASSWORD': Env['PGPASS'],
        'USER': Env['PGUSER'],
    }
}


STATIC_URL='/static/'
STATIC_ROOT=os.path.join(BASE_DIR, 'staticfiles')
