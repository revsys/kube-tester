# vim: ft=python
# pylint: disable=unused-wildcard-import,missing-docstring,wildcard-import,unused-import,undefined-variable,relative-beyond-top-level

from os import environ as Env
from kube_tester.settings import *



SECRET_KEY = 'abc123'

DATABASES = {
    'default': {
        'ATOMIC_REQUESTS': False,
        'AUTOCOMMIT': True,
        'CONN_MAX_AGE': 0,
        'ENGINE': 'django_db_geventpool.backends.postgresql_psycopg2',
        'HOST': 'localhost',
        'USER': 'postgres',
        'NAME': Env['PGDB'],
    }
}

