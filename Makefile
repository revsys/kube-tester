.PHONY: image push-image build-image

include .version

ifdef PKG_PROXY
	PROXY_ARGS := --build-arg=http_proxy=${PKG_PROXY} --build-arg=https_proxy=${PKG_PROXY}
else
	PROXY_ARGS :=
endif

ifndef CI_BUILD_REF_NAME
	CI_BUILD_REF_NAME := latest
endif

ifndef LOCALREG
	LOCALREG := kube-tester
endif


build-image:
	@echo building ${IMAGE_TAG}
	for el in tini builder-py builder-js; do \
		docker build ${PROXY_ARGS} -t ${LOCALREG}/$$el:${CI_BUILD_REF_NAME} -f docker/$$el .;  \
	done
	@docker build ${PROXY_ARGS} --build-arg=LOCALREG=${LOCALREG} --build-arg=CI_BUILD_REF_NAME=${CI_BUILD_REF_NAME} -t ${IMAGE_TAG} -f docker/final .

push-image:
	@echo pushing ${IMAGE_TAG}
	@docker push ${IMAGE_TAG}

image: build-image push-image



